# Client

#### Clone the repo

````
git clone git@bitbucket.org:adam_ibrahim/pizzitta-client.git
````

### env
create the .env file

```
cp .env.example .env
```
modify url for endpoint

e.g if you run the api server with laravel homestead
then your url http://localhost:8000/api

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

#### User Credentials
There is a ready created user for testing
````
'email':'info@pizzitta.test', 'password':'password'
````
