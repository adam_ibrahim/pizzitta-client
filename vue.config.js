const path = require('path');
module.exports = {
  publicPath: process.env.VUE_APP_BASE_URL,
  lintOnSave: false,
  configureWebpack: {
    resolve: {
      alias: {
        assets: path.join(__dirname, 'src/assets'),
      },
    },
  },
  devServer: {
    port: 8080,
    headers: { 'Access-Control-Allow-Origin': '*' },
    hot: true,
    inline: true,
    historyApiFallback: true,
    noInfo: false,
    compress: false,
    clientLogLevel: 'warn',
    publicPath: 'http://localhost:8080/',
  },
};
