import Vue from 'vue';
import VueRouter from 'vue-router';
import Orders from '../views/Orders';
import Cart from '../views/Cart';
import Confirm from '../views/Confirm';
import Signin from '../views/Signin';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Pages/Home'),
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: Signin,
    beforeEnter: (to, from, next) => {
      if (store.getters['auth/authenticated']) {
        return next({
          name: 'Home',
        });
      }
      next();
    },
  },
  {
    path: '/orders',
    name: 'Orders',
    component: Orders,
    beforeEnter: (to, from, next) => {
      if (!store.getters['auth/authenticated']) {
        return next({
          name: 'SignIn',
        });
      }
      next();
    },
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart,
    beforeEnter: (to, from, next) => {
      if (!store.getters['auth/authenticated']) {
        return next({
          name: 'SignIn',
        });
      }
      next();
    },
  },
  {
    path: '/contacts',
    name: 'Contacts',
    component: () => import('@/views/Pages/Contacts'),
    beforeEnter: (to, from, next) => {
      if (!store.getters['auth/authenticated']) {
        return next({
          name: 'SignIn',
        });
      }
      next();
    },
  },

  {
    path: '/confirm',
    name: 'Confirm',
    component: Confirm,
    beforeEnter: (to, from, next) => {
      if (!store.getters['auth/authenticated']) {
        return next({
          name: 'SignIn',
        });
      }
      next();
    },
  },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
