export default {
  methods: {
    ADD_NOTICE(options) {
      let resOptions = options;
      if (typeof options === 'string') resOptions = { body: options };
      this.$bvToast.toast(resOptions.body, {
        title: resOptions.title || 'Notice',
        toaster: 'b-toaster-bottom-right',
        solid: true,
        appendToast: true,
        autoHideDelay: 3000,
        variant: options.variant || options.type || undefined,
        ...resOptions,
      });
    },
  },
};
