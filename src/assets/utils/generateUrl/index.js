
/**

  * @param {Object} constantsObject - constant object
  * @return {Function} funtion for generate constant
  **/
export default function (constantsObject = null) {
  let urls = {};
  if (constantsObject !== null) {
    urls = constantsObject;
  }

  /**

    * @param {String} constant - name of constants
    * @param {Object} myReplace - params for replace
    * @param {Object} myReplace.params - params ':somethink' in url
    * @return {String} resulst url
    **/
  return function (constant, myReplace = {}) {
    let result = urls[constant] || null;
    if (result && Object.keys(myReplace).length > 0) {
      const params = myReplace.params || {};
      const query = myReplace.query || {};
      // first replace :somethnk part in string
      for (const param in params) {
        result = result.replace(':' + param, params[param]);
      }

      // add query in result string
      if (Object.keys(query).length > 0) {
        const addQueryString = [];
        for (const param in query) {
          if (query[param])addQueryString.push(param + '=' + encodeURIComponent(query[param]));
        }
        if (addQueryString.length > 0) {
          result += result.includes('?') ? '&' : '?';
          result += addQueryString.join('&');
        }
      }
    }
    return result;
  };
}
