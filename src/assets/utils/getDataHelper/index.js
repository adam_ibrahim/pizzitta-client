import generateUrlConstructor from 'assets/utils/generateUrl';
import axios from 'axios';
/**
 * Helper for requests
 *
 * @param {Object} - object with constant
 * @return {Function} functions for request to server
 */
export default function (constantsObject = null) {
  const generateUrl = generateUrlConstructor(constantsObject);

  /**
  * @param {String,Object} urlObj -name of constant or object i.e {url:nameOfConstant,params:{id:item,id}}
  * @param {String,Object} settings -parametrs of request or name of method
  * @param {Object} data - data payload for server
  * @return {Promise}  promise with request to server
  **/
  return async function (urlObj, settings = 'GET', data = {}) {
    let url = null;
    let params = null;
    let query = null;
    // if url or constant
    if (typeof urlObj === 'string') {
      // if constant
      if (urlObj.indexOf('/') === -1) url = generateUrl(urlObj);
      // if link
      else url = urlObj;
    } else if (typeof urlObj === 'object') {
      // if need generate
      url = urlObj.url;
      params = urlObj.params || null;
      query = urlObj.query || null;
      url = generateUrl(url, { params, query });
    }

    // if second parametr is string
    if (typeof settings === 'string') {
      settings = {
        method: settings,
      };
    }

    let res = null;
    let resEnd = { success: false, data: null };
    try {
      const sendData = calcOptions({
        url: url,
        data: data,
        ...settings,
      });
      res = await axios(sendData);
      resEnd = { success: true, data: res.data };
    } catch (e) {
      resEnd = { ...resEnd, ...hendlerError(e) };
    }
    return resEnd;
  };
}

/**
 * standart error object
 *
 * @param {Response Error}
 * @return {Object} {error:String,errors:Object,code:500 || response.status}
 */
function hendlerError(error) {
  const res = { error: null, errors: {}, code: 500 };
  const response = error.response;
  if (typeof response === 'object') {
    res.code = response.status;
    // если на верхнем уровне респонса есть проперти status
    if (response.status !== undefined) {
      res.errors = response.data || response.errors;
      if (response.status >= 500) {
        res.error = 'Error from server';
      }
    } else if (response.errors !== undefined) {
      res.errors = response.errors;
    } else if (response.message) {
      res.error = response.message;
    } else {
      res.errors = response;
    }
  } else if (typeof response === 'undefined') {
    res.error = 'Error from server';
  }
  return res;
}

function calcOptions(options = {}) {
  let axiosDefaultOpt = {
    dataType: 'json',
  };

  if (options.method.toLowerCase() !== 'get') {
    if (options.data && Object.keys(options.data).length === 0 && typeof options.data.append === 'function') { // if send FormData
      axiosDefaultOpt.headers = { 'Content-Type': 'multipart/form-data' };
    }
  }
  axiosDefaultOpt = { ...axiosDefaultOpt, ...options };
  return axiosDefaultOpt;
}
