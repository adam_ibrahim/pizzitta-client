import '@babel/polyfill';
import 'mutationobserver-shim';
import Vue from 'vue';
import './plugins/bootstrap-vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import notice from 'assets/mixins/notice.js';

require('@/store/subscriber');

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL;
axios.defaults.headers.common.currency = window.localStorage.getItem('currency')

Vue.config.productionTip = false;
Vue.mixin(notice);
store.dispatch('auth/attempt', localStorage.getItem('token')).then(() => {
  // remove loader
  const elLoader = document.getElementById('initial-loader');
  elLoader.style.opacity = 0;
  setTimeout(() => {
    elLoader.parentNode.removeChild(elLoader);
  }, 0.3 * 1000);

  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app');
});
