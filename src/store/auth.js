import axios from 'axios';
import * as _ from '@/store/modules/PizzaList/mutations-types.js';
export default {
  namespaced: true,
  state: {
    token: null,
    user: null,
  },

  getters: {
    authenticated(state) {
      return state.token && state.user;
    },

    user(state) {
      return state.user;
    },
  },

  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_USER(state, data) {
      state.user = data;
    },
  },

  actions: {
     signIn({ dispatch }, credentials) {
      	return axios.post('auth/login', credentials).
	       then(response => { dispatch('attempt', response.data.token); return response; })
    	   .catch(error => { return Promise.reject(error.response); });	
    },

    async attempt({ commit, state }, token) {
      if (token) {
        commit('SET_TOKEN', token);
      }
      if (!state.token) {
        return;
      }
      try {
        const response = await axios.get('auth/me');
        commit('SET_USER', response.data.data);
        commit('PizzaList/' + _.SET_CART, response.data.data.cart, { root: true });
      } catch (e) {
        commit('SET_TOKEN', null);
        commit('SET_USER', null);
      }
    },

    signOut({ commit }) {
      return axios.post('auth/logout').then(() => {
        commit('SET_TOKEN', null);
        commit('SET_USER', null);
      });
    },
  },
};
