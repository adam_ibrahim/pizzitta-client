import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import PizzaList from './modules/PizzaList';
import Order from './modules/Order';
import Main from './modules/Main';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    Main,
    PizzaList,
    Order,
  },
});
