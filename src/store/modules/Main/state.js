export default function () {
  // get from localStorage for fast get. Anyway will send request for update list
  const localCurrencies = window.localStorage.getItem('currencies');
  const currentCurrencyId = window.localStorage.getItem('currency') || 1;
  return {
    currencies: localCurrencies ? JSON.parse(localCurrencies) : null,
    currentCurrencyId: +currentCurrencyId,
  };
}
