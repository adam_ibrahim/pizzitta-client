import * as _ from './mutations-types';
import axios from 'axios';
export default {
  [_.SET_CURRENCIES](state, currencies) {
    state.currencies = currencies;
    // save for fast get after reload
    window.localStorage.setItem('currencies', JSON.stringify(currencies));
  },
  [_.SET_CURRENCY](state, currencyId) {
    state.currentCurrencyId = currencyId;
    window.localStorage.setItem('currency', currencyId);
    axios.defaults.headers.common.currency = currencyId;
  },
};
