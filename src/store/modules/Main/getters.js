
export default {
  currentCurrency(state, getters) {
    return (state.currencies || []).find(i => i.id === getters.currentCurrencyId) || null;
  },
  currentCurrencyId(state, getters) {
    return state.currentCurrencyId;
  },
};
