import getDataHelper from 'assets/utils/getDataHelper';

import * as $ from './actions-types';
import * as _ from './mutations-types';
import urls from './urls';
// import testApi from './test-api';
const GET_DATA = getDataHelper(urls);

export default {
  async [$.LOAD_CURRENCIES]({ state, commit, dispatch }, data) {
    const res = await GET_DATA('currenciesList');
    if (res.success) {
      commit(_.SET_CURRENCIES, res.data.data);
    }
    return res;
  },
};
