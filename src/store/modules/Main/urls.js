
export default {
  currenciesList: '/currency/list',
  setCurrency: '/currency/change/:id',
};
