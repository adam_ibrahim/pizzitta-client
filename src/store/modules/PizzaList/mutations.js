import * as _ from './mutations-types';
export default {
  [_.SET_PIZZA_LIST](state, pizzaList) {
    state.pizzaList = pizzaList;
  },
  [_.SET_PIZZA_RESPONSE](state, pizzaListResponse) {
    state.pizzaListResponse = pizzaListResponse;
  },
  [_.SET_CART](state, cart) {
    state.cart = cart;
  },
  [_.SET_SIGNIN_CART_ITEM](state, item) {
    state.signinCartItem= item;
  },
};
