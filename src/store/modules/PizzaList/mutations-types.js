/**
 * set pizza
 */
export const SET_PIZZA_LIST = 'SET_PIZZA_LIST';
/**
 *
/**
 * set pizza response
 */
export const SET_PIZZA_RESPONSE = 'SET_PIZZA_RESPONSE';

/**
 * set cart of user
 */
export const SET_CART = 'SET_CART';

/**
 * set cart of user
 */
export const SET_SIGNIN_CART_ITEM = 'SET_SIGNIN_CART_ITEM';
