export default function () {
  return {
    pizzaList: [],
    pizzaListResponse: {},
    loadedList: false,
    loadingList: false,
    cart: null,
    cartLoading: false,
    signinCartItem:{},
    modals: [
    ],
  };
}
