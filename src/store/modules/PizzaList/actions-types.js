/**
 * load pizzas
 */
export const LAOD_PIZZA_ITEMS = 'LAOD_PIZZA_ITEMS';
/**
 * Add item to cart
 */
export const ADD_TO_CART = 'ADD_TO_CART';

/**
 * get cart info
 */
export const GET_CART = 'GET_CART';

/**
 * update item cart
 */
export const UPDATE_ITEM_IN_CART = 'UPDATE_ITEM_IN_CART';

/**
 * remove item from cart
 */
export const REMOVE_ITEM_IN_CART = 'REMOVE_ITEM_IN_CART';

/**
 * Add signin item to cart
 */
export const SET_SIGNIN_CART_ITEM = 'SET_SIGNIN_CART_ITEM';
