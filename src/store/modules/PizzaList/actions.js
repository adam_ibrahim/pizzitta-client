import getDataHelper from 'assets/utils/getDataHelper';

import * as $ from './actions-types';
import * as _ from './mutations-types';
import urls from './urls';
// import testApi from './test-api';
const GET_DATA = getDataHelper(urls);

export default {
  async [$.LAOD_PIZZA_ITEMS]({ state, commit, dispatch }, query) {
    state.loadingList = true;
    const res = await GET_DATA({ url: 'pizzaList', query });
    // const res = { success: true, data: testApi.pizzaList };
    if (res.success) {
      commit(_.SET_PIZZA_LIST, res.data.data);
      commit(_.SET_PIZZA_RESPONSE, res.data);
      state.loadedList = true;
    }
    state.loadingList = false;
    return res;
  },
  async [$.ADD_TO_CART]({ state, commit, dispatch }, data) {
    const res = await GET_DATA('cartList', 'post', {
      items: [data],
    });
    if (res.success) {
      // reload orders after add new
      dispatch($.GET_CART);
    }
    return res;
  },
  async [$.GET_CART]({ state, commit, dispatch }, query = {}) {
    state.cartLoading = true;
    const res = await GET_DATA('cartList');
    if (res.success) {
      commit(_.SET_CART, res.data.data);
    }
    state.cartLoading = false;
    return res;
  },
  async [$.UPDATE_ITEM_IN_CART]({ state, commit, dispatch }, options) {
    const res = await GET_DATA({ url: 'RUDcart', params: { id: options.id } }, 'patch', {
      quantity: options.quantity,
    });
    if (res.success) {
      commit(_.SET_CART, res.data.data);
    }
    return res;
  },
  async [$.REMOVE_ITEM_IN_CART]({ state, commit, dispatch }, id) {
    const res = await GET_DATA({ url: 'RUDcart', params: { id } }, 'delete');
    if (res.success) {
      commit(_.SET_CART, res.data.data);
    }
    return res;
  },
  [$.SET_SIGNIN_CART_ITEM]({ state, commit}, item) {
      commit(_.SET_SIGNIN_CART_ITEM, item);
  },
};
