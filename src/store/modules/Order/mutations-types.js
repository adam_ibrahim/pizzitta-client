/**
 * set orders
 */
export const SET_ORDERS_LIST = 'SET_ORDERS_LIST';
/**
 *
/**
 * set orders response
 */
export const SET_ORDERS_RESPONSE = 'SET_ORDERS_RESPONSE';
