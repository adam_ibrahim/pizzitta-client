import getDataHelper from 'assets/utils/getDataHelper';

import * as $ from './actions-types';
import * as $PizzaList from '@/store/modules/PizzaList/actions-types.js';
import * as _ from './mutations-types';
import urls from './urls';
const GET_DATA = getDataHelper(urls);

export default {
  async [$.LOAD_ORDERS]({ state, commit, dispatch }, query) {
    state.loadingList = true;
    const res = await GET_DATA({ url: 'ordersList', query });
    if (res.success) {
      commit(_.SET_ORDERS_LIST, res.data.data);
      commit(_.SET_ORDERS_RESPONSE, res.data);
      state.loadedList = true;
    }
    state.loadingList = false;
    return res;
  },
  async [$.ADD_CONTACT]({ state, commit, dispatch }, form) {
    return GET_DATA('contact', 'post', form);
  },
  async [$.CREATE_ORDER]({ state, commit, dispatch }, form) {
    const res = await GET_DATA('ordersList', 'post', form);
    if (res.success) {
      dispatch('PizzaList/' + $PizzaList.GET_CART, undefined, { root: true });
    }
    return res;
  },
};
