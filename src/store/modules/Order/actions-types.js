/**
 * load orders
 */
export const LOAD_ORDERS = 'LOAD_ORDERS';
/**
 * add contact
 */
export const ADD_CONTACT = 'ADD_CONTACT';
/**
 * add order
 */
export const CREATE_ORDER = 'CREATE_ORDER';
