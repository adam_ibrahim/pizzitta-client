import * as _ from './mutations-types';
export default {
  [_.SET_ORDERS_LIST](state, ordersList) {
    state.ordersList = ordersList;
  },
  [_.SET_ORDERS_RESPONSE](state, ordersListResponse) {
    state.ordersListResponse = ordersListResponse;
  },
};
