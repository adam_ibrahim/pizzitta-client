import store from '@/store';
import axios from 'axios';

store.subscribe((mutation) => {
  switch (mutation.type) {
  case 'auth/SET_TOKEN':
    if (mutation.payload) {
      axios.defaults.headers.common.Authorization = `Bearer ${mutation.payload}`;
      //axios.interceptors.request.use((config) => {
	//    config.params = config.params || {};
	//    config.params['token'] = mutation.payload;
	//    return config;
	//});
	axios.defaults.headers.common['X-Access-Token'] = `Bearer ${mutation.payload}`;		
      localStorage.setItem('token', mutation.payload);
    } else {
      axios.defaults.headers.common.Authorization = null;
      localStorage.removeItem('token');
    }
    break;
  }
});
